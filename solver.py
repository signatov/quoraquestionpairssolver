#!/usr/bin/env python
# -*- coding: utf-8
# Author: Sergey Ignatov
# Copyright (c) 2017
# License: MIT License
"""Quora Question Pairs solver.
Where else but Quora can a physicist help a chef with a math problem and get cooking tips in return?
Quora is a place to gain and share knowledge—about anything. It’s a platform to ask questions and
connect with people who contribute unique insights and quality answers. This empowers people to
learn from each other and to better understand the world.
Over 100 million people visit Quora every month, so it's no surprise that many people ask similarly
worded questions. Multiple questions with the same intent can cause seekers to spend more time
finding the best answer to their question, and make writers feel they need to answer multiple
versions of the same question. Quora values canonical questions because they provide a better
experience to active seekers and writers, and offer more value to both of these groups in the
long term.
Currently, Quora uses a Random Forest model to identify duplicate questions. In this competition,
Kagglers are challenged to tackle this natural language processing problem by applying advanced
techniques to classify whether question pairs are duplicates or not. Doing so will make it easier to
find high quality answers to questions resulting in an improved experience for Quora writers,
seekers, and readers.
"""

from collections import defaultdict
from itertools import combinations
import os.path
import subprocess
import time
import numpy as np
import pandas as pd
from sklearn.externals import joblib
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, log_loss
from sklearn.model_selection import train_test_split, KFold
from sklearn.preprocessing import StandardScaler
import spacy
import spacy.parts_of_speech
from nltk.tokenize import sent_tokenize, RegexpTokenizer
from nltk.stem.snowball import EnglishStemmer
from shallowlearn.models import GensimFastText
from tqdm import tqdm

N_ROWS = None
TRAIN_FILE = './train.csv'
TEST_FILE = './test.csv'
RESULT_FILE = './submit.csv'
MODEL_FILE = './model.pkl'
GENSIM_MODEL_FILE = './gensim_model.pkl'

def measure(method):
    """Measure decorator."""
    def run(*args, **kwargs):
        """Run method and measure execution time, print it to console."""
        start = time.time()
        result = method(*args, **kwargs)
        execution_time = time.time() - start
        if execution_time > 100:
            print '%r %.2f min' % (method.__name__, execution_time / 60.0)
        else:
            print '%r %.2f sec' % (method.__name__, execution_time)
        return result
    return run

def get_nouns(text, nlp):
    """Get English nouns from text."""

    def is_noun(elem):
        """Check if elem is spaCy token and it is noun."""
        return isinstance(elem, spacy.tokens.Token) and elem.pos == spacy.parts_of_speech.NOUN

    doc = nlp(text)
    return filter(is_noun, doc)

def count_nouns(text, nlp):
    """Count noun words in text."""
    return len(get_nouns(text, nlp))

def norm_count_nouns(text, nlp):
    """Call function count_nouns and normalize result."""
    nouns_counter = count_nouns(text, nlp)
    return nouns_counter if nouns_counter > 0.0 else 1.0

@measure
def load_train_data():
    """Load and return train data set."""
    data = pd.read_csv(TRAIN_FILE,
                       dtype={'question1': np.str, 'question2': np.str}, nrows=N_ROWS)
    data['test_id'] = -1
    return data

@measure
def load_test_data():
    """Load and return test data set."""
    data = pd.read_csv(TEST_FILE,
                       dtype={'question1': np.str, 'question2': np.str}, nrows=N_ROWS)
    data['id'] = -1
    data['qid1'] = -1
    data['qid2'] = -1
    data['is_duplicate'] = -1
    return data

@measure
def merge_data(train_data, test_data):
    """Merge train and test data sets."""
    def merge_list(list1, list2):
        """Merge two lists."""
        list_ = list1
        list_.extend(list2)
        return list_

    data = pd.concat([train_data, test_data])
    data['question1'] = data['question1'].fillna('')
    data['question2'] = data['question2'].fillna('')
    data['word_list'] = data.apply(
        lambda r: merge_list(r['question1'].decode('utf-8').split(' '),
                             r['question2'].decode('utf-8').split(' ')), axis=1)
    data['uid'] = np.arange(data.shape[0])
    data = data.set_index(['uid'])
    return data

@measure
def make_introsect_feature(data):
    """Calculate introsection feature."""
    def intersect(row):
        """Introsection feature."""
        question1 = row['question1']
        question2 = row['question2']
        return len(set(questions_dict[question1]).
                   intersection(set(questions_dict[question2])))

    questions_dict = defaultdict(set)

    count = data.shape[0]
    with tqdm(desc='Questions dict', total=count) as pbar:
        for _, row in data.iterrows():
            question1 = row['question1']
            question2 = row['question2']
            questions_dict[question1].add(question2)
            questions_dict[question2].add(question1)
            pbar.update(1)

    data['q1_q2_intersect'] = data.progress_apply(intersect, axis=1)
    return ['q1_q2_intersect']

@measure
def make_questions_len_feature(data):
    """Make column with lengths of first and second questions."""
    data['len1'] = data['question1'].str.len().astype(np.float32)
    data['len2'] = data['question2'].str.len().astype(np.float32)
    data['abs_diff_lens'] = np.abs(data['len1'] - data['len2'])
    data['log_abs_diff_lens'] = np.log(data['abs_diff_lens'] + 1)
    return ['len1', 'len2', 'abs_diff_lens', 'log_abs_diff_lens']

@measure
def make_first_word_feature(data):
    """Make feature that compare first word of questions."""
    data['is_first_word_equal'] = data.progress_apply(
        lambda r: int(r['question1'].split(' ')[0] == r['question2'].split(' ')[0]), axis=1)
    return ['is_first_word_equal']

@measure
def make_spacy_feature(data):
    """Count noun words in questions."""
    # Don't forget `python -m spacy download en_core_web_md` before.
    nlp = spacy.load('en_core_web_md') # spacy.load('en')

    data['nouns1'] = data.progress_apply(
        lambda r: norm_count_nouns(r['question1'].decode('utf-8'), nlp), axis=1)
    data['nouns2'] = data.progress_apply(
        lambda r: norm_count_nouns(r['question2'].decode('utf-8'), nlp), axis=1)
    data['ratio_nouns'] = data['nouns1'] / data['nouns2']
    return ['nouns1', 'nouns2', 'ratio_nouns']

@measure
def make_gensim_fasttext_feature(data, ix_train):
    """Use fastText algo to predict."""
    if os.path.isfile(GENSIM_MODEL_FILE):
        clf = joblib.load(GENSIM_MODEL_FILE)
    else:
        clf = GensimFastText(size=100, min_count=1, loss='hs', iter=2)
        clf.fit(data['word_list'][ix_train], data['is_duplicate'][ix_train])
        joblib.dump(clf, GENSIM_MODEL_FILE, compress=9)
    pred = clf.predict(data['word_list'])
    data['gensim_fasttext'] = pred
    return ['gensim_fasttext']

def similarity(text1, text2):
    """Similarity two pieces of text."""
    if not len(text1) or not len(text2):
        return 0.0
    return len(text2.intersection(text2))/(1.0 * (len(text1) + len(text2)))

def textrank(text1, text2):
    """TextRank function."""
    text = text1 + '\n' + text2
    sentences = sent_tokenize(text)
    tokenizer = RegexpTokenizer(r'\w+')
    stemmer = EnglishStemmer()
    words = [set(stemmer.stem(word) for word in tokenizer.tokenize(sentence.lower()))
             for sentence in sentences]
    pairs = combinations(range(len(sentences)), 2)
    scores = [(i, j, similarity(words[i], words[j])) for i, j in pairs]
    return scores[0][2] if scores else 0.0

@measure
def make_textrank_feature(data):
    """Calculate textrank for two questions."""
    data['textrank'] = data.progress_apply(
        lambda r: textrank(r['question1'].decode('utf-8'), r['question1'].decode('utf-8')), axis=1)
    return ['textrank']

@measure
def make_predict_model(x_train, y_train):
    """Create and setup prediction model."""
    if os.path.isfile(MODEL_FILE):
        return joblib.load(MODEL_FILE)
    model = LogisticRegression(n_jobs=16)
    model = model.fit(x_train, y_train)
    joblib.dump(model, MODEL_FILE, compress=9)
    return model

@measure
def predict(model, x_test):
    """Make prediction."""
    predictions = model.predict_proba(x_test)
    return predictions[:, 1]

@measure
def calc_accuracy(model, x_train, y_train):
    """Split train data, make cross validation and calc accuracy factor."""
    xx_train, xx_test, yy_train, yy_test = train_test_split(x_train, y_train,
                                                            test_size=0.5, random_state=1)
    scaler = StandardScaler()
    scaler.fit(x_train)
    # xx_train_std = scaler.transform(xx_train)
    # xx_test_std = scaler.transform(xx_test)
    xx_train_std = xx_train
    xx_test_std = xx_test
    model.fit(xx_train, yy_train)
    pred = model.predict(xx_test)
    print 'Misclassified samples: %d' % (yy_test != pred).sum()
    print 'Accuracy: %.2f' % accuracy_score(yy_test, pred)
    # print 'Log loss: %.2f' % log_loss(yy_test, pred)

@measure
def kfold(model, x_train, y_train):
    """Use KFold to calculate mean accuracy score."""
    cross_validator = KFold(n_splits=10)
    fold = 0
    outcomes = []
    for train_index, test_index in cross_validator.split(x_train, y_train):
        fold += 1
        xx_train, xx_test = x_train.values[train_index], x_train.values[test_index]
        yy_train, yy_test = y_train.values[train_index], y_train.values[test_index]
        model.fit(xx_train, yy_train)
        pred = model.predict(xx_test)
        accuracy = accuracy_score(yy_test, pred)
        outcomes.append(accuracy)
        print 'Fold %d accuracy: %.2f' % (fold, accuracy)
    mean_accuracy = np.mean(outcomes)
    print 'Mean accuracy score: %.2f' % mean_accuracy

@measure
def make_submit(x_test, prediction):
    """Make submit csv file and save it."""
    df_submit = x_test.copy()
    df_submit['is_duplicate'] = prediction
    df_submit[['test_id', 'is_duplicate']].head()
    df_submit[['test_id', 'is_duplicate']].to_csv(RESULT_FILE, index=False)
    subprocess.call(['gzip', '-k', '-f', '-6', RESULT_FILE])

@measure
def main():
    """Main solver code."""
    tqdm.pandas()
    df_train = load_train_data()
    df_test = load_test_data()
    data = merge_data(df_train, df_test)
    del(df_train, df_test)
    ix_train = data['is_duplicate'] >= 0
    ix_test = data['is_duplicate'] == -1
    ix_is_dup = np.where(data['is_duplicate'] == 1)[0]
    ix_not_dup = np.where(data['is_duplicate'] == 0)[0]

    features = []
    features.extend(make_questions_len_feature(data))
    features.extend(make_first_word_feature(data))
    features.extend(make_spacy_feature(data))
    features.extend(make_gensim_fasttext_feature(data, ix_train))
    features.extend(make_textrank_feature(data))
    features.extend(make_introsect_feature(data))
    data[features].head()
    print data.dtypes

    model = make_predict_model(data[features][ix_train], data['is_duplicate'][ix_train])
    calc_accuracy(model, data[features][ix_train], data['is_duplicate'][ix_train])
    kfold(model, data[features][ix_train], data['is_duplicate'][ix_train])
    prediction = predict(model, data[features][ix_test])
    make_submit(data.loc[ix_test], prediction)

if __name__ == '__main__':
    main()
